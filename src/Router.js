export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];
	static #menuElement;

	static navigate(path, pushHistory) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
			Array.from(document.querySelector('.mainMenu').children).forEach(
				element => {
					if (element.children[0].getAttribute('href') === path) {
						element.children[0].classList.add('active');
					} else {
						element.children[0].classList.remove('active');
					}
				}
			);
			if (!pushHistory) {
				window.history.pushState(null, null, path);
			}
		}
	}

	static set menuElement(element) {
		this.#menuElement = element;

		Array.from(this.#menuElement.children).forEach(element => {
			element.addEventListener('click', event => {
				// ici pas de problème de scope, `this` est préservé
				event.preventDefault();
				console.log(
					this.navigate(event.currentTarget.children[0].getAttribute('href'))
				); // ok
			});
		});
	}
}
