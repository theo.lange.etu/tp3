import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const form = element,
			input = form.querySelector('input[name]');

		form.addEventListener('submit', function (event) {
			event.preventDefault();
			if (input.value != '') {
				console.log('La Pizza ' + input.value + ' a été ajoutée !');
				input.value = '';
				this.submit(event);
			} else {
				alert('Fo Reflechir Hein !');
			}
		});
	}
}
