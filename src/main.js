import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import PizzaForm from './pages/PizzaForm';
import Component from './components/Component';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

pizzaList.pizzas = data;

Router.navigate(document.location.pathname);

document.querySelector('section').setAttribute('style', 'display:');

document
	.querySelector('button.closeButton')
	.addEventListener('click', event => {
		// ici pas de problème de scope, `this` est préservé
		event.preventDefault();
		document.querySelector('section').setAttribute('style', 'display:none'); // ok
	});

Router.menuElement = document.querySelector('.mainMenu');

window.onpopstate = function (event) {
	Router.navigate(document.location.pathname, true);
};
